
/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of kcca-lib.
 *
 *  kcca-lib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  kcca-lib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with kcca-lib.  If not, see <http://www.gnu.org/licenses/>.
 */                                          

#include "kcButtons.h"

void KCButton::init()
{
  pinMode(_pin, INPUT_PULLUP);
}

void KCButton::buttonPressed(uint8_t state_)
{
  doButtonPressed();
}

void KCButton::buttonReleased(uint8_t state_)
{
  doButtonReleased();
}

void KCButton::read()
{
  uint8_t read = digitalRead(_pin); 

  if (read == PIN_UP && _state == PIN_DOWN) {
    buttonPressed(read);
  }
  else if (read == PIN_DOWN && _state == PIN_UP) {
    buttonReleased(read);
  }

  setState(read);
}

void KCButton::setState(uint8_t state_)
{
  if (_state == state_) return;
  _state = state_;
  _stateChangeTime = millis();
}


void kcButtons_init(BUTTON_TYPE buttons_[], size_t nbOfPins_) {
  // Serial.println(nbOfPins_);
  for (short i = 0; i < nbOfPins_; i++) {
    pinMode(buttons_[i]._pin, PIN_MODE);
  }
}

