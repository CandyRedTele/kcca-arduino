/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of kcca-lib.
 *
 *  kcca-lib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  kcca-lib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with kcca-lib.  If not, see <http://www.gnu.org/licenses/>.
 */                                          


// add symlink $ARDUINO_HOME/libraries/kcca-lib
#ifndef KCBUTTONS_H_
#define KCBUTTONS_H_
#include "Arduino.h"


// PIN_MODE defaults to INPUT_PULLUP
#ifndef PIN_MODE
#define PIN_MODE INPUT_PULLUP
#endif

#define PIN_DOWN ((PIN_MODE == INPUT_PULLUP) ? 1 : 0)
#define PIN_UP   ((PIN_MODE == INPUT_PULLUP) ? 0 : 1)


class KCButton {
public:
  KCButton(short pin_, const char* msg_, const char* msgReleased_ = nullptr) : 
    _pin(pin_), _msgPressed(msg_), _msgReleased(msgReleased_), _state(PIN_DOWN), _stateChangeTime(millis()) {}
  void init();

  /**
   * will read the digital pin and trigger buttonPressed, buttonReleased
   *
   **/
  void read();

  /*
   * to use getTimeSinceLastStateChangeInMillis you must use setState so the last 
   * state change time is saved.
   */
  unsigned long getTimeSinceLastStateChangeInMillis() { return millis() - _stateChangeTime; }


  uint8_t getState() { return _state; }
  uint8_t _pin;

protected:
  unsigned long _stateChangeTime;
  uint8_t _state;
  const char* _msgPressed;
  const char* _msgReleased;

  // TODO the do* functions should be stored as function pointers so we can set the callback on the client site
  virtual void doButtonReleased() { 
    if (!_msgReleased) return;
    Serial.println(_msgReleased);
  }

  virtual void doButtonPressed() {
    Serial.println(_msgPressed);
  }

private:
  void buttonPressed(uint8_t state_);
  void buttonReleased(uint8_t state_);

  void setState(uint8_t state_);

};

#ifdef KC_SCANCODE
class KCScanCodeButton : public KCButton {
public:
  KCScanCodeButton(short pin_, const char* msg_, const char* msgReleased_) : 
                                    KCButton(pin_, msg_, msgReleased_), _idx(0), _idxRel(0) {}

protected:
  void doButtonPressed() override {
    if (!_msgPressed) return;
    Keyboard.press(_msgPressed[_idx]);
    Keyboard.release(_msgPressed[_idx]);
    // cycle trough characters in _msgPressed
    _idx = (_idx + 1) % strlen(_msgPressed);
  }

  void doButtonReleased() override {
    if (!_msgReleased) return;
    Keyboard.press(_msgReleased[_idxRel]);
    Keyboard.release(_msgReleased[_idxRel]);
    // cycle trough characters in _msgReleased
    _idxRel = (_idxRel + 1) % strlen(_msgReleased);
  }

private:
  uint8_t _idx;
  uint8_t _idxRel;
};
#endif

// using the Keyboard library will add  ~1600 bytes to the executable size
#ifdef KC_SCANCODE
#include "Keyboard.h"
#define BUTTON_TYPE KCScanCodeButton
#else
#define BUTTON_TYPE KCButton
#endif


// client should define the buttons array and NB_OF_PINS macro as follow:
//
//  BUTTON_TYPE buttons[] = {
//    { pin#, "pressed string", "released string" },
//  };
// #define NB_OF_PINS (sizeof(buttons)/sizeof(buttons[0]))

void kcButtons_init(BUTTON_TYPE buttons_[], size_t nbOfPins_);



#endif
