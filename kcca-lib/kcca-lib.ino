/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of kcca-lib.
 *
 *  kcca-lib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  kcca-lib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with kcca-lib.  If not, see <http://www.gnu.org/licenses/>.
 */                                          


#define KC_SCANCODE
#include "kcButtons.h"

#define LED_PIN 13

KCButton b(2, "Hi");

void setup()
{
    b.init();
    pinMode(13, OUTPUT);
}



void loop()
{
    b.read();

    Serial.println(b.getTimeSinceLastStateChangeInMillis());

    if (b.getState() == 0) {
        digitalWrite(LED_PIN, HIGH);
    } else {
        digitalWrite(LED_PIN, LOW);
    }

    delay(500);
}

