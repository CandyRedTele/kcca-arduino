#!/bin/bash

if [[ -z "$1" ]]; then
  echo "donne-moi un folder"
  exit 1
fi

FOLDER=$1
VERSION=1.0

cp kcca-lib/kcButtons.*  $FOLDER/
zip -r ${FOLDER}-${VERSION}.zip ${FOLDER} makefile.global
rm $FOLDER/kcButtons.*

