/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of enigma.
 *
 *  enigma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  enigma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with enigma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEDS_H_ 
#define LEDS_H_

#include "Adafruit_NeoPixel.h"

// 7 if using the Jewels
#define NB_OF_PIXELS_PER_LED 7

Adafruit_NeoPixel pixels_6 = Adafruit_NeoPixel(7*6, 6, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_7 = Adafruit_NeoPixel(7*6, 7, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_8 = Adafruit_NeoPixel(7*6, 8, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_9 = Adafruit_NeoPixel(7*6, 9, NEO_GRBW + NEO_KHZ800);

struct Strip {
  uint8_t pin;
  uint8_t nb_of_leds;
};

class Leds {
  public:
    Leds(Strip strips_[], Adafruit_NeoPixel pixels_[], uint8_t nbOfStrips_) : _strips(strips_), _pixels(pixels_), _nbOfStrips(nbOfStrips_),
      tot_pix(0){}

    void init() {
      _pixels[0] = pixels_6;
      _pixels[1] = pixels_7;
      _pixels[2] = pixels_8;
      _pixels[3] = pixels_9;

      for (int i = 0; i < _nbOfStrips; i++) {
      // DOESNT WORK!!! BUG !!!  _pixels[i] = Adafruit_NeoPixel(_strips[i].nb_of_leds * NB_OF_PIXELS_PER_LED, _strips[i].pin, NEO_GRBW + NEO_KHZ800);
         _pixels[i].begin();
         tot_pix += _strips[i].nb_of_leds * NB_OF_PIXELS_PER_LED;
      }

      toBlack();
    }

    void setLedColor(uint8_t buttonIdx, uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
      int idx = buttonIdx;
      bool done = false;

      // Serial.println("setLedColor()");
      for (int i = 0; !done && i < _nbOfStrips; i++) {
        if (idx < _strips[i].nb_of_leds) {
          int pix_start = idx*NB_OF_PIXELS_PER_LED;
          for (int j = 0; j < NB_OF_PIXELS_PER_LED; j++) {
            _pixels[i].setPixelColor(pix_start+j, r, g, b, w);
          }

          //Serial.println(i);
          
          _pixels[i].show();
          done = true;
        } else {
          idx -= _strips[i].nb_of_leds;
        }
      }
    }

    void toBlack() {
      toRGBW(0,0,0,0);
    }

    void toWhite() {
      toRGBW(255, 255, 255, 255);
    }

    void toRed() {
      toRGBW(255, 0, 0, 0);
    }

    void toGreen() {
      toRGBW(0, 255, 0, 0);
    }

    void toRGBW(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
      for (int i = 0; i < _nbOfStrips; i++) {
        for (int j = 0; j < _strips[i].nb_of_leds*NB_OF_PIXELS_PER_LED; j++) {
          _pixels[i].setPixelColor(j, r, g, b/*,WHITE*/);
        }

        _pixels[i].show();
      }
    }

  private:
    Strip* _strips;
    Adafruit_NeoPixel* _pixels;
    uint8_t _nbOfStrips;
    uint8_t tot_pix;
};

#define NB_OF_STRIPS 4
static Strip strips[NB_OF_STRIPS] = {
  {6, 6},
  {7, 6},
  {8, 6},
  {9, 6},
};
static Adafruit_NeoPixel pixels[NB_OF_STRIPS];
static Leds leds(strips, pixels, NB_OF_STRIPS);


Leds& Leds_get_leds() {
  return leds;
}

#endif
