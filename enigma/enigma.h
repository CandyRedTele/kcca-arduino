/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of enigma.
 *
 *  enigma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  enigma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with enigma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENIGMA_H_
#define ENIGMA_H_
#include "Arduino.h"

#include "kcButtons.h"
#include "leds.h"

#include <Wire.h> // Enable this line if using Arduino Uno, Mega, etc.
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
static Adafruit_7segment matrix = Adafruit_7segment();


// NOTE: in KC_SCANCODE mode, if the pressed/released strings 
//       are > 1 characters, we cycle through all characters.
//       example: for pressed string "ABC"
//                  1st press => scan code is 'A'
//                  2nd press => scan code is 'B'
//                  3rd press => scan code is 'C'
//                  4th press => scan code is 'A'
//                  ...
BUTTON_TYPE buttons[] = {
//{ pin#, pressed string, released string }
// 25 boutons + le start bouton
//  {48, "0", "0R"},
  {22, "1", "1R"},
  {23, "2", "2R"},
  {24, "3", "3R"},
  {25, "4", "4R"},
  {26, "5", "5R"},
  {27, "6", "6R"},
  {28, "7", "7R"},
  {29, "8", "8R"},
  {30, "9", "9R"},
  {31, "10", "10R"},
  {32, "11", "11R"},
  {33, "12", "12R"},
  {34, "13", "13R"},
  {35, "14", "14R"},
  {36, "15", "15R"},
  {37, "16", "16R"},
  {38, "17", "17R"},
  {39, "18", "18R"},
  {40, "19", "19R"},
  {41, "20", "20R"},
  {42, "21", "21R"},
  {43, "22", "22R"},
  {44, "23", "23R"},
  {45, "24", "24R"},
  {47, "START", "START_REL"}, // START SHOULD BE LAST
};
#define NB_OF_PINS (sizeof(buttons)/sizeof(buttons[0]))
#define NB_OF_KEYS (NB_OF_PINS - 1)
#define BUTTONS_START_IDX  (NB_OF_PINS - 1)
#define LOOP_DELAY_MS 10

#define UNCONNECTED_PIN 0
class Enigma
{
  public:
    Enigma(BUTTON_TYPE buttons_[], uint8_t nbOfButtons_, unsigned int secGameLength_) : 
                                   _started(false), 
                                   _buttons(buttons_),
                                   _nbOfButtons(nbOfButtons_), 
                                   _msGameLength(secGameLength_*1000), 
                                   _expired(false),
                                   _leds(Leds_get_leds())
    {}

    void init() {
      kcButtons_init(_buttons, NB_OF_PINS);
      randomSeed(analogRead(UNCONNECTED_PIN));
      _leds.init();
      matrix.begin(0x70);
      matrix.print(_msGameLength/10, DEC);
      matrix.writeDigitRaw(2, 2);
      matrix.writeDisplay();
    }

    void stop() {
      Serial.println("Engima stopping...");
      _started = false;
      _leds.toBlack();
      matrixToZero();

    }

    void start(bool restart_ = false) { 
      if ((!restart_ && _buttons[BUTTONS_START_IDX].getState() == PIN_UP)
          || restart_) {

        if (!restart_) {
          _started = true;
          Serial.println("Engima starting...");
        } else {
          Serial.println("Engima restarting...");
        }

        _leds.toWhite();
        updateNextWinner();
        Serial.print("next winner is idx:");
        Serial.println(_nextWinner);

        // timer
        _msLastStart = millis();
        matrix.print(_msGameLength/10, DEC);
        matrix.writeDigitRaw(2, 2);
        matrix.writeDisplay();
      }
    }

    bool isStarted() { return _started; }

    void animLost() {
      /*
      for (int i = 0; i < 3; i++) {
        _leds.toRed();
        delay(500);
        _leds.toWhite();
        delay(500);
      }*/

    }

    void animWin() {
      delay(2500);
      /*
      for (int i = 0; i < 3; i++) {
        _leds.toGreen();
        delay(500);
        _leds.toWhite();
        delay(500);
      }*/
    }



    void update() {
      readButtonStates();
      if (!isStarted()) return;
        
      checkTimer();

      if (_expired) {
        animLost();
        _expired = false;
        //start(true /*restart*/);
        stop();
      } else {
        // check if there is a winner
        for (int i = 0; i < NB_OF_KEYS; i++) {
          if (_buttons[i].getState() == PIN_UP) {
            if (_nextWinner == i) {
              // !!! WINNER !!!
              _leds.setLedColor(i, 0, 0, 255, 0);
              animWin();
              //start(true /*restart*/);
              stop();
              break;
            } else {
              _leds.setLedColor(i, 255, 0, 0, 0);
            }
          }
        }
      }
    }

  private:

    void readButtonStates() {
      _buttons[BUTTONS_START_IDX].read();
      
      for (short i = 0; i < NB_OF_KEYS; i++) {
        _buttons[i].read();
      }
    }

    void matrixToZero() {
      matrix.drawColon(false);
      matrix.writeDigitNum(1, 0, false);
      matrix.writeDigitRaw(2, 2);
      matrix.writeDigitNum(3, 0, false);
      matrix.writeDigitNum(4, 0, false);
      matrix.writeDisplay();
    }

    void checkTimer() {
      int diffMS = millis() - _msLastStart;
      if (diffMS > _msGameLength) {
        _expired = true;
        Serial.println("expired");
        matrixToZero();
        return;
      }

      int toPrint = (_msGameLength - diffMS)/10;
      matrix.print(toPrint, DEC); // TODO improve this thing
      if (toPrint < 100) {
        matrix.writeDigitNum(1, 0, false);
      }
      matrix.writeDigitRaw(2, 2);
      matrix.writeDisplay();
    }

    void updateNextWinner() {
      // [,NB_OF_PINS-1[
      _nextWinner = random(0, NB_OF_PINS-1);
    }

    bool _started;
    BUTTON_TYPE* _buttons;
    uint8_t _nbOfButtons;
    uint8_t _nextWinner; // this is the index in the _buttons array

    Leds& _leds;

    // timer
    unsigned long _msGameLength;
    unsigned long _msLastStart;
    bool _expired;

};

#endif
