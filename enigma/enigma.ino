/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of enigma.
 *
 *  enigma is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  enigma is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with enigma.  If not, see <http://www.gnu.org/licenses/>.
 */


//#define KC_SCANCODE
#include "enigma.h"


static Enigma enigma{buttons, NB_OF_PINS, 5 /* sec game length */};

void setup()
{
  Serial.begin(9600);
  enigma.init();
}


void loop()
{
  enigma.update();

  // Starting the game for the first time
  if (!enigma.isStarted()) {
    enigma.start();
  }

  delay(LOOP_DELAY_MS);
}


