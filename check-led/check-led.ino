
//#include "FastLED.h"

#include <Adafruit_NeoPixel.h>
//CRGB leds[7];
//

#define NB_OF_LEDS 6

Adafruit_NeoPixel pixels_6 = Adafruit_NeoPixel(7*NB_OF_LEDS, 6, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_7 = Adafruit_NeoPixel(7*NB_OF_LEDS, 7, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_8 = Adafruit_NeoPixel(7*NB_OF_LEDS, 8, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel pixels_9 = Adafruit_NeoPixel(7*NB_OF_LEDS, 9, NEO_GRBW + NEO_KHZ800);

void setup()
{
   pixels_6.begin(); // This initializes the NeoPixel library.
   pixels_6.show(); // Initialize all pixels to 'off'

   pixels_7.begin(); // This initializes the NeoPixel library.
   pixels_7.show(); // Initialize all pixels to 'off'

   pixels_8.begin(); // This initializes the NeoPixel library.
   pixels_8.show(); // Initialize all pixels to 'off'

   pixels_9.begin(); // This initializes the NeoPixel library.
   pixels_9.show(); // Initialize all pixels to 'off'
}



void doit(int r, int g, int b, int w) {


  for (int i = 0; i < 7*6; i++) {
    pixels_6.setPixelColor(i, r, g, b, w);
  }
  pixels_6.show();

  for (int i = 0; i < 7*6; i++) {
    pixels_7.setPixelColor(i, r, g, b, w);
  }
  pixels_7.show();

  /*
  for (int i = 0; i < 7*6; i++) {
    pixels_8.setPixelColor(i, r, g, b, w);
  }
  pixels_8.show();
  */

  for (int i = 0; i < 7*6; i++) {
    pixels_9.setPixelColor(i, r, g, b, w);
  }
  pixels_9.show();
}

bool done = false;

void loop()
{
  if (!done) {
    doit(255, 0, 0, 0);
  }

  delay(1000 * 10);
}


