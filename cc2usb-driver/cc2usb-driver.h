/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of cc2usb-driver.
 *
 *  cc2usb-driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  cc2usb-driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cc2usb-driver.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CC2USB_DRIVER_H_
#define CC2USB_DRIVER_H_
#include "Arduino.h"

#define PIN_MODE INPUT_PULLUP
#include "kcButtons.h"

#define LOOP_DELAY_MS 10

#ifndef ENIGMA_32
#include "config.h"
#else
#include "config-enigma-32.h"
#endif

#define NB_OF_PINS (sizeof(buttons)/sizeof(buttons[0]))
#endif
