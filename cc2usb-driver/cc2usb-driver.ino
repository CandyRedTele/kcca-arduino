/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of cc2usb-driver.
 *
 *  cc2usb-driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  cc2usb-driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cc2usb-driver.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *
 * When a button is pressed, a string is written to the USB dev
 * The string should be written ONCE until the button is released
 *
 **/

//#define KC_SCANCODE

// uncomment to use the 32 buttons config (see config-enigma-32.h)
// NB the default config file is for 12 buttons (see config.h)
// #define ENIGMA_32

#include "cc2usb-driver.h"

void setup()
{
  Serial.begin(9600);
  kcButtons_init(buttons, NB_OF_PINS);
}


void loop()
{
  uint8_t read;
  for (short i = 0; i < NB_OF_PINS; i++) {
    KCButton& b = buttons[i];
    b.read();
  }

  delay(LOOP_DELAY_MS);
}


