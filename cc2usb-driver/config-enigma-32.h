/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of cc2usb-driver.
 *
 *  cc2usb-driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  cc2usb-driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cc2usb-driver.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CC2USB_CONFIG_32_H_
#define CC2USB_CONFIG_32_H_

// NOTE: in KC_SCANCODE mode, if the pressed/released strings 
//       are > 1 character, we cycle through all characters.
//       example: for pressed string "ABC"
//                  1st press => scan code is 'A'
//                  2nd press => scan code is 'B'
//                  3rd press => scan code is 'C'
//                  4th press => scan code is 'A'
//                  ...
BUTTON_TYPE buttons[] = {
  // { pin#, pressed string, released string }
  {22,  "PIN_22_PRESS", "PIN_22_RELEASE"},
  {23,  "PIN_23_PRESS", "PIN_23_RELEASE"},
  {24,  "PIN_24_PRESS", "PIN_24_RELEASE"},
  {25,  "PIN_25_PRESS", "PIN_25_RELEASE"},
  {26,  "PIN_26_PRESS", "PIN_26_RELEASE"},
  {27,  "PIN_27_PRESS", "PIN_27_RELEASE"},
  {28,  "PIN_28_PRESS", "PIN_28_RELEASE"},
  {29,  "PIN_29_PRESS", "PIN_29_RELEASE"},
  {30,  "PIN_30_PRESS", "PIN_30_RELEASE"},
  {31,  "PIN_31_PRESS", "PIN_31_RELEASE"},
  {32,  "PIN_32_PRESS", "PIN_32_RELEASE"},
  {33,  "PIN_33_PRESS", "PIN_33_RELEASE"},
  {34,  "PIN_34_PRESS", "PIN_34_RELEASE"},
  {35,  "PIN_35_PRESS", "PIN_35_RELEASE"},
  {36,  "PIN_36_PRESS", "PIN_36_RELEASE"},
  {37,  "PIN_37_PRESS", "PIN_37_RELEASE"},
  {38,  "PIN_38_PRESS", "PIN_38_RELEASE"},
  {39,  "PIN_39_PRESS", "PIN_39_RELEASE"},
  {40,  "PIN_40_PRESS", "PIN_40_RELEASE"},
  {41,  "PIN_41_PRESS", "PIN_41_RELEASE"},
  {42,  "PIN_42_PRESS", "PIN_42_RELEASE"},
  {43,  "PIN_43_PRESS", "PIN_43_RELEASE"},
  {44,  "PIN_44_PRESS", "PIN_44_RELEASE"},
  {45,  "PIN_45_PRESS", "PIN_45_RELEASE"},
  {46,  "PIN_46_PRESS", "PIN_46_RELEASE"},
  {47,  "PIN_47_PRESS", "PIN_47_RELEASE"},
  {48,  "PIN_48_PRESS", "PIN_48_RELEASE"},
  {49,  "PIN_49_PRESS", "PIN_49_RELEASE"},
  {50,  "PIN_50_PRESS", "PIN_50_RELEASE"},
  {51,  "PIN_51_PRESS", "PIN_51_RELEASE"},
  {52,  "PIN_52_PRESS", "PIN_52_RELEASE"},
  {53,  "PIN_53_PRESS", "PIN_53_RELEASE"},
};

#endif
