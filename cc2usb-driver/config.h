/*
 *  Copyright (C) 2018 Joseph-Antoine Martineau-Gagné  
 *
 *  This file is part of cc2usb-driver.
 *
 *  cc2usb-driver is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  cc2usb-driver is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cc2usb-driver.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CC2USB_CONFIG_H_
#define CC2USB_CONFIG_H_

// NOTE: in KC_SCANCODE mode, if the pressed/released strings 
//       are > 1 character, we cycle through all characters.
//       example: for pressed string "ABC"
//                  1st press => scan code is 'A'
//                  2nd press => scan code is 'B'
//                  3rd press => scan code is 'C'
//                  4th press => scan code is 'A'
//                  ...
BUTTON_TYPE buttons[] = {
  // { pin#, pressed string, released string }
  {1,  "PIN_1_PRESS", "PIN_1_RELEASE"},
  {2,  "PIN_2_PRESS", "PIN_2_RELEASE"},
  {3,  "PIN_3_PRESS", "PIN_3_RELEASE"},
  {4,  "PIN_4_PRESS", "PIN_4_RELEASE"},
  {5,  "PIN_5_PRESS", "PIN_5_RELEASE"},
  {6,  "PIN_6_PRESS", "PIN_6_RELEASE"},
  {7,  "PIN_7_PRESS", "PIN_7_RELEASE"},
  {8,  "PIN_8_PRESS", "PIN_8_RELEASE"},
  {9,  "PIN_9_PRESS", "PIN_9_RELEASE"},
  {10, "PIN_10_PRESS", "PIN_10_RELEASE"},
  {11, "PIN_11_PRESS", "PIN_11_RELEASE"},
  {12, "PIN_12_PRESS", "PIN_12_RELEASE"},
};

#endif
